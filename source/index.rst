.. Archaeogenetics documentation master file, created by
   sphinx-quickstart on Wed Apr 24 11:50:34 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

###################
Ancient DNA session
###################

Welcome to the Ancient DNA session of the advanced `course`_ "Archaeogenetics approaches to investigate domestication and evolution", CIBIO, Porto (20-24 May 2019), by Claudio Ottoni (Sapienza University of Rome).

  .. _course: https://cibio.up.pt/workshops--courses/details/advanced-course-archaeogenetics-approaches-to-investigate-domestication-and-evolution

********
Contents
********

.. toctree::
   :maxdepth: 2
   :numbered:
   
   1_ListTools
   2_ReadsFiltering
   3_PreparationReference
   4_ReadsMapping_v2
   5_SummaryReports
   6_DamageAnalysis
   7_VariantsCall


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
